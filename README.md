**Logowanie**

Login: test@test.pl

Hasło: testowy

---

## Kilka słów o tym co i jak zostało wykonane

W projekcie w celach demonstracyjnych użyłem wielu rzeczy nad wyrost.

1. Użyte są komponenty klasowe oraz komponenty funkcyjne (wystarczył by jeden rodzaj)
2. Zastosowane są reguły kiedy komponent może ponownie się wyrenderować a w przypadku komponentow funkcyjnych, kiedy powinien obliczyć pasek postępu (przy tak małym projekcie nie ma to sensu).
3. Użyty jest redux (przy tak małym projekcie nie ma to sensu. Jak również Context czy useReducer)
4. Zaimplementowany został osobny moduł do obsługi logowanie Firebase
5. Zastosowany został osobny moduł do obsługi async-storage (napisałem go wcześniej na potrzeby innego projektu), tutaj mi się tylko przydał.

Ja z reguły na rozmowach pytam o takie rzeczy więc i tutaj postanowiłem je ująć.

---

## Co może wam wydać się dziwne jeśli zajrzycie do serwisu Storage

1. Jest to Multiton. Aby móc używać metod serwisu, należy najpierw stworzyć / odwołać się do kontenera - rozwiązanie takie pozwala używać tych samych nazw kluczy w ramach różnch kontenerów.
2. Zwraca Promise i przyjmuje callbacki - lubię móc użyć wyniku metody w różny sposób.

---

## Co i gdzie

1. W katalogu "artifact" znajduje się wersja produkcyjna aplikacji.
2. W katalogu "doc" znajdują się zrzuty ekranu oraz nagranie działania aplikacji.

---

## Samo zadanie

Zadanie zostało wykonane w taki sposób, że:

1. Użytkownik może się zalogować
2. Widzi swój harmonogram kiedy powinien wypić szklankę wody
3. Klika klawisz "Wypij szklankę wody" i na harmonogrami jest to oznaczone
4. Wszyskto zapisywane jest w async-storage więc jeśli "ubije" aplikacje nie straci danych

Czas kiedy użytkownik może odkliknąć wypicie szklanki wody "nie jest pilnowany", w przeciwnym razie trudno było by wam przetestować bo na każde następne kliknięcie musielibyście czekać 2h.

---

## Podsumowanie

Żeby aplikacjia faktycznie przypominała o wypiciu szklani wody a nie jedynie pokazywała po uruchomieniu jej czy powinno się to zrobić czy nie, wówczas powienien być użyty Firebase Cloud Messaging:

1. Aplikacja powinna w foreground i background sprawdzać czy użytkownik nie powinien już napić się wody i wysyłać powiadomienie FCM (rozwiązanie średnie ze względu na baterię i proces w tle)
2. Wysyłać powiadomienie FCM powinien backend

To już nie jest robota na 4h tylko trochę więcej więc założyłem, że obecna implementacja wystarczy.