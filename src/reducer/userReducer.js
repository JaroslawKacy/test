
import {
  SIGN_IN,
  SIGN_OUT,
} from '../constant';

const initialState = {
  loggedInUser: null,
};

const userReducer = (state = initialState, action) => {
  switch(action.type) {
    case SIGN_IN:
      return {
        ...state,
        loggedInUser: action.payload,
      };

    case SIGN_OUT:
      return {
        ...state,
        loggedInUser: null,
      };

    default:
      return state;
  }
}

export default userReducer;
