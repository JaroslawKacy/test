import {
  combineReducers,
  createStore
} from 'redux';

import userReducer from './reducer/userReducer';

const rootReducer = combineReducers({
  userStore: userReducer
});

const Store = () => {
  return createStore(rootReducer);
};

export default Store;
