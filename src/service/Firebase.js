
import firebase from 'react-native-firebase';

/**
 * @type    {Function}
 * @returns {Object}
 */
const auth = function() {
  return firebase.auth();
}

/**
 * @type    {Function}
 * @param   {string}          email
 * @param   {string}          password
 * @param   {callbackSuccess} [callbackSuccess = () => {}]
 * @param   {callbackError}   [callbackError = () => {}]
 * @returns {Promise}
 */
const createUserWithEmailAndPassword = function(email, password, callbackSuccess = () => {}, callbackError = () => {}) {
  return new Promise((resolve, reject) => {
    auth()
      .createUserWithEmailAndPassword(email, password)
      .then(result => {
        callbackSuccess(result);
        resolve(result);
      })
      .catch(error => {
        callbackError(error);
        reject(error);
      })
    ;
  });
}

/**
 * @type    {Function}
 * @param   {callback} [callback = () => {}]
 * @returns {Function}
 */
const onAuthStateChanged = function(callback = () => {}) {
  return auth().onAuthStateChanged(result => {
    callback(result);
  });
}

/**
 * @type    {Function}
 * @param   {string}          email
 * @param   {string}          password
 * @param   {callbackSuccess} [callbackSuccess = () => {}]
 * @param   {callbackError}   [callbackError = () => {}]
 * @returns {Promise}
 */
const signInWithEmailAndPassword = function(email, password, callbackSuccess = () => {}, callbackError = () => {}) {
  return new Promise((resolve, reject) => {
    auth()
      .signInWithEmailAndPassword(email, password)
      .then(result => {
        callbackSuccess(result);
        resolve(result);
      })
      .catch(error => {
        callbackError(error);
        reject(error);
      })
    ;
  });
}

/**
 * @type    {Function}
 * @param   {callback} [callback = () => {}]
 * @returns {void}
 */
const signOut = function(callback = () => {}) {
  auth().signOut();
  callback();
}

export {
  auth,
  createUserWithEmailAndPassword,
  onAuthStateChanged,
  signInWithEmailAndPassword,
  signOut,
};
