
import AsyncStorage from '@react-native-community/async-storage';

/**
 * @type    {Function}
 * @returns {{CONST: {NO, YES}, remove, use}}
 */
export default Storage = (function(storage) {

  /**
   * @const
   * @type      {Object}
   * @property  {string} NO 
   * @property  {string} YES
   */
  const CONST = Object.freeze({
    NO:   'no',
    YES:  'yes',
  });

  /**
   * @type  {Object}
   */
  const instances = {};

  /**
   * @constructor
   * @type        {Function}
   * @param       {string}  name
   * @param       {Object}  storage
   * @returns     {{getAllKeys, getItem, hasItem, multiRemove, removeItem, setItem}}
   */
  const Constructor = function(name, storage) {

    /**
     * @type {Object}
     */
    const scope = {

      /**
       * @type {string}
       */
      name: name,

      /**
       * @type {Object}
       */
      storage: storage,
    }

    /**
     * @type    {Function}
     * @returns {Object}
     */
    this.getScope = function() {
      return scope;
    }
  };

  Constructor.prototype = (function() {

    /**
     * @type    {Function}
     * @param   {Function}  [callback = () => {}]
     * @returns {Promise}
     */
    const getAllKeys = function(callback = () => {}) {
      return this.getScope().storage.getAllKeys((error, result) => callback(error, result));
    }

    /**
     * @type    {Function}
     * @param   {string}    key
     * @param   {Function}  callback
     * @returns {(Promise | null)}
     */
    const getItem = function(key, callback) {
      return this.getScope().storage.getItem(
        prepareKey.call(this, key),
        (error, result) => {
          if (callback !== undefined) {
            if (isNaN(result)) {
              try {
                result = JSON.parse(result);
              } catch(error) {
                result = result;
              }
            }

            if (result === null) {
              result = undefined;
            }

            callback(error, result);
          }
        }
      );
    }

    /**
     * @type    {Function}
     * @param   {string}    key
     * @param   {Function}  [callbackSuccess = () => {}]
     * @param   {Function}  [callbackError = () => {}]
     * @returns {Promise}
     */
    const hasItem = function(key, callbackSuccess = () => {}, callbackError = () => {}) {
      return new Promise((resolve, reject) => {
        this.getScope().storage.getAllKeys((error, keys) => {
          if (error) {
            callbackError(error);
            reject(error);
          }

          if (keys.includes(prepareKey.call(this, key))) {
            callbackSuccess(true);
            resolve(true);
          } else {
            callbackSuccess(false);
            resolve(false);
          }
        });
      });
    }

    /**
     * @type    {Function}
     * @param   {Array}     keys
     * @param   {Function}  [callback = () => {}]
     * @returns {Promise}
     */
    const multiRemove = function(keys, callback = () => {}) {
      return this.getScope().storage.multiRemove(
        prepareKeys.call(this, keys),
        error => callback(error))
      ;
    }

    /**
     * @type    {Function}
     * @param   {string}    key
     * @param   {Mixed}     value
     * @param   {Function}  [callback = () => {}]
     * @returns {(Promise | null)}
     */
    const setItem = function(key, value, callback = () => {}) {
      if (value === undefined) {
        throw new Error(`The Value of "${key}" key is required!`);
      }

      value = (typeof value === 'object')
        ? JSON.stringify(value)
        : value
      ;

      return this.getScope().storage.setItem(
        prepareKey.call(this, key),
        value,
        error => callback(error)
      );
    }

    /**
     * @type    {Function}
     * @param   {string}    key
     * @param   {Function}  [callback = () => {}]
     * @returns {Promise}
     */
    const removeItem = function(key, callback = () => {}) {
      return this.getScope().storage.removeItem(
        prepareKey.call(this, key),
        error => callback(error)
      );
    }

    /**
     * @type    {Function}
     * @param   {string}  key
     * @returns {string}
     */
    const prepareKey = function(key) {
      const scope = this.getScope();

      return `@${scope.name}_${key}`;
    }

    /**
     * @type    {Function}
     * @param   {Array} keys
     * @returns {Array}
     */
    const prepareKeys = function(keys) {
      let newKeys = [];

      keys.forEach(key => {
        newKeys.push(
          prepareKey.call(this, key)
        );
      });

      return newKeys
    }

    return {
      getAllKeys,
      getItem,
      hasItem,
      multiRemove,
      removeItem,
      setItem,
    };

  })();

  /**
   * @type    {Function}
   * @param   {string}  name
   * @throws  {Error}
   * @returns {void}
   */
  const remove = function(name) {
    try {
      if (name === undefined) {
        throw new Error(`The User name is required!`);
      }

      if (instances.hasOwnProperty(name)) {
        delete instances[name];
      }
    } catch(error) {
      console.error(error);
    }
  }

  /**
   * @type    {Function}
   * @param   {string}  name
   * @throws  {Error}
   * @returns {{getAllKeys, getItem, multiRemove, setItem, removeItem}}
   */
  const use = function(name) {
    try {
      if (name === undefined) {
        throw new Error(`The User name is required!`);
      }

      if (!instances.hasOwnProperty(name)) {
        instances[name] = new Constructor(
          name,
          storage
        );
      }

      return instances[name];
    } catch(error) {
      console.error(error);
    }
  }

  return {
    CONST,
    remove,
    use,
  };

})(AsyncStorage);
