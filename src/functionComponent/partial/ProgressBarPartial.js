
import React, {
  useEffect,
  useMemo,
  useState,
} from 'react'

import {
  ProgressBarAndroid,
  StyleSheet,
  Text,
  View,
} from 'react-native';

import PropTypes from 'prop-types';

export default ProgressBarPartial = ({glassesOfWater, schedule}) => {
  const [indeterminate, setIndeterminate] = useState(true);

  let progress = useMemo(() => {
    return glassesOfWater.length / schedule.length;
  }, [glassesOfWater.length]);

  useEffect(() => {
    setIndeterminate(true);
    setTimeout(() => setIndeterminate(false), 1300);
  }, [progress]);

  return (
    <View style={styleSheet.viewContainer}>
      <View style={styleSheet.viewRow}>
        <Text style={styleSheet.textHours}>
          {schedule[0]}
        </Text>
        <ProgressBarAndroid
          indeterminate={indeterminate}
          progress={progress}
          style={styleSheet.progressBar}
          styleAttr='Horizontal'
        />
        <Text style={styleSheet.textHours}>
          {schedule[schedule.length - 1]}
        </Text>
      </View>

      <View style={[styleSheet.viewRow, styleSheet.viewHead]}>
        <Text style={[styleSheet.textSchedule, styleSheet.textHead]}>
          When you should drink
        </Text>
        <Text style={[styleSheet.textSchedule, styleSheet.textHead]}>
          When you drank
        </Text>
      </View>

      {schedule.map((item, index) => {
        return (
          <View
            key={index} 
            style={styleSheet.viewRow}
          >
            <Text style={styleSheet.textSchedule}>
              {item}
            </Text>
            <Text style={styleSheet.textSchedule}>
              {glassesOfWater[index] ? glassesOfWater[index] : ''}
            </Text>
          </View>
        );
      })}
    </View>
  );
};

ProgressBarPartial.propTypes = {
  glassesOfWater: PropTypes.array,
  schedule:       PropTypes.array
}

ProgressBarPartial.defaultProps = {
  glassesOfWater: [],
  schedule:       []
}

const styleSheet = StyleSheet.create({
  progressBar: {
    flex: 1,
  },
  textHead: {
    fontSize: 18,
  },
  textSchedule: {
    flex:       0.5,
    textAlign:  'center',
  },
  textHours: {
    textAlign:  'center',
    width:      70,
  },
  viewContainer: {
    flex:           1,
    justifyContent: 'center',
  },
  viewHead: {
    marginVertical: 10,
  },
  viewRow: {
    flexDirection:  'row',
    justifyContent: 'center',
    width:          '90%'
  }
});
