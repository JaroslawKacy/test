
import { SIGN_IN } from '../constant';

export const setUser = user => {
  return {
    type:     SIGN_IN,
    payload:  user
  };
}
