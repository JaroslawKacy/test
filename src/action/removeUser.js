
import { SIGN_OUT } from '../constant';

export const removeUser = user => {
  return {
    type:     SIGN_OUT,
    payload:  user
  };
}
