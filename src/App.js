
import React, {
  useEffect,
  useRef,
  useState
} from 'react'

import {
  NavigationContainer
}  from '@react-navigation/native';

import {
  createStackNavigator
} from '@react-navigation/stack';

import { Provider } from 'react-redux';

import LoginScreen          from './classComponent/LoginScreen';
import MainScreen           from './classComponent/MainScreen';
import {onAuthStateChanged} from './service/Firebase';
import Store                from './store';

const store = Store()
const Stack = createStackNavigator();

const App = () => {
  const [user, setUser] = useState(null);
  const navigationRef   = useRef();

  useEffect(() => {
    const unsubscribe = onAuthStateChanged(result => setUser(result));

    navigationRef.current.navigate(user ? 'Main' : 'Login');

    return () => {
      unsubscribe();
    };
  }, [user]);

  return (
    <Provider store={store}>
      <NavigationContainer
        ref={navigationRef}
        initialRouteName='Login'
      >
        <Stack.Navigator>
          {!user ? (
            <Stack.Screen
              name='Login'
              component={LoginScreen}
            />
          ) : (
            <Stack.Screen
              name='Main'
              component={MainScreen}
            />
          )}
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
}

export default App
