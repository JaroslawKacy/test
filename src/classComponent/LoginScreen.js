
import React from 'react'

import {
  Button,
  StyleSheet,
  Text,
  TextInput,
  View,
} from 'react-native'

import { signInWithEmailAndPassword } from '../service/Firebase';

export default class LoginScreen extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      email:        '',
      password:     '',
      errorMessage: null,
    };
  }

  _handleLogin = () => {
    const {email, password} = this.state

    signInWithEmailAndPassword(
      email,
      password,
      () => this.props.navigation.navigate('Main'),
      error => this.setState({errorMessage: error.message})
    );
  }

  render() {
    const {
      email,
      password
    } = this.state;

    const disabled = email && password ? false : true;

    return (
      <View style={styleSheet.viewContainer}>
        <Text style={styleSheet.textLogin}>Login</Text>
        <TextInput
          autoCapitalize='none'
          keyboardType='email-address'
          onChangeText={email => this.setState({email})}
          placeholder='E-mail'
          style={styleSheet.textInput}
          value={email}
        />
        <TextInput
          autoCapitalize='none'
          onChangeText={password => this.setState({password})}
          placeholder='Password'
          secureTextEntry
          style={styleSheet.textInput}
          value={password}
        />
        <View style={styleSheet.viewButton}>
          <Button
            disabled={disabled}
            onPress={this._handleLogin}
            title='Login'
          />
        </View>
        {this.state.errorMessage &&
          <Text style={styleSheet.textError}>
            {this.state.errorMessage}
          </Text>
        }
      </View>
    )
  }
}

const styleSheet = StyleSheet.create({
  textError: {
    color:      '#f00',
    marginTop:  20,
    width:      '90%',
  },
  textInput: {
    borderColor:  '#333',
    borderWidth:  1,
    height:       40,
    marginBottom: 10,
    width:        '90%',
  },
  textLogin: {
    fontSize:     20,
    marginBottom: 10,
  },
  viewContainer: {
    alignItems:     'center',
    flex:           1,
    justifyContent: 'center',
  },
  viewButton: {
    width: 100,
  }
});
