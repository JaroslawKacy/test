import React from 'react'

import {
  Button,
  StyleSheet,
  Text,
  View,
} from 'react-native'

import { connect }  from 'react-redux';
import moment       from 'moment';

import { setUser }        from '../action/setUser';
import { removeUser }     from '../action/removeUser';
import ProgressBarPartial from '../functionComponent/partial/ProgressBarPartial';
import { auth, signOut }  from '../service/Firebase';
import Storage            from '../service/Storage';

class MainScreen extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      glassesOfWater: [],
      schedule:       [
        '0:00 AM',
        '2:00 AM',
        '4:00 AM',
        '6:00 AM',
        '8:00 AM',
        '10:00 AM',
        '12:00 AM',
        '2:00 PM',
        '4:00 PM',
        '6:00 PM',
        '8:00 PM',
        '10:00 PM'
      ]
    };

    this.storage = Storage.use('water');
  }

  componentDidMount() {
    this._setUser();
    this._checkWaterStorage();
  }

  componentWillUnmount() {
    this.storage = null;
  }

  shouldComponentUpdate(nextProps, nextState) {
    return this.state.glassesOfWater !== nextState.glassesOfWater
      ? true
      : false
    ;
  }

  _setUser = () => {
    const {currentUser} = auth();

    this.props.set(currentUser);
  }

  _checkWaterStorage = () => {
    this.storage.getItem('glassesOfWater', (error, result) => this.setState({glassesOfWater: result ? result : []}));
  }

  _signOut = () => {
    signOut(() => this.props.remove());
  }

  _manageGlassOfWater = () => {
    this.setState(prevState => {
      const glassesOfWater = [...prevState.glassesOfWater];

      glassesOfWater.push(
        moment(new Date()).format('LTS')
      );

      if (glassesOfWater.length > this.state.schedule.length) {
        return {glassesOfWater: []};
      }

      return {glassesOfWater: glassesOfWater}
    }, () => this.storage.setItem('glassesOfWater', this.state.glassesOfWater));
  }

  render() {
    const {
      glassesOfWater,
      schedule
    } = this.state;

    const title = glassesOfWater.length === schedule.length
      ? 'Clean'
      : 'Drink the glass of water!'
    ;

    return (
      <View style={styleSheet.viewContainer}>
        <View style={[styleSheet.viewRow, styleSheet.viewRowUser]}>
          <View style={styleSheet.viewUser}>
          {this.props.loggedInUser && 
            <Text>
              User: {this.props.loggedInUser.email}
            </Text>
          }
          </View>
          <View style={styleSheet.viewSignOut}>
            <Button
              onPress={this._signOut}
              title='SignOut'
            />
          </View>
        </View>
        <ProgressBarPartial
          glassesOfWater={glassesOfWater}
          schedule={schedule}
        />
        <View style={styleSheet.viewDrink}>
          <Button
            onPress={this._manageGlassOfWater}
            title={title}
          />
        </View>
      </View>
    )
  }
}

const styleSheet = StyleSheet.create({
  viewContainer: {
    alignItems: 'center',
    flex:       1,
  },
  viewDrink: {
    height: 100,
    width:  250,
  },
  viewSignOut: {
    width: 100,
  },
  viewRow: {
    flexDirection:  'row',
    width:          '90%'
  },
  viewRowUser: {
    marginVertical: 10,
  },
  viewUser: {
    flex: 1,
  }
});


const mapStateToProps = state => {
  return {
    loggedInUser: state.userStore.loggedInUser
  }
}

const mapDispatchToProps = dispatch => {
  return {
    set:    user  => dispatch(setUser(user)),
    remove: ()    => dispatch(removeUser())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(MainScreen)
